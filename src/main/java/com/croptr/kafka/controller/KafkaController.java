package com.croptr.kafka.controller;

import com.croptr.kafka.storage.Kafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
    @Autowired
    KafkaTemplate<String, Kafka> kafkaKafkaTemplate;
    @RequestMapping(value = "/send",method = RequestMethod.POST)
    public void postToKafka(@RequestBody Kafka kafka){
        kafkaKafkaTemplate.send("myTopic",kafka);
    }

    @KafkaListener(topics = "myTopic")
    public void getFromKafka(Kafka kafka){
        System.out.println(kafka.toString());
    }
}

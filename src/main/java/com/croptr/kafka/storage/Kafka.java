package com.croptr.kafka.storage;

import lombok.Data;

import java.io.Serializable;
import java.io.SerializablePermission;

@Data
public class Kafka {
    private String name;
    private String message;
}
